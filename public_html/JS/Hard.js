/*******************************************************************************
 SNAKE JAVASCRIPT Hard
 *******************************************************************************/



/*******************************************************************************
 VARIABLES
 *******************************************************************************/

//Varibles store information that we can acess and modify//

var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var Highscore = localStorage.getItem(HighscoreX);
var HighscoreX;

var food;


var context;
var screenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;
var playMenu;
var playButton;

var image;
var image2;

var GameOver;
var Grow;
var High;
 
 var Music;
/*******************************************************************************
 EXECUTING GAME CODE
 *******************************************************************************/

gameInitialize();
snakeInitialize();
foodInitialize();
setInterval(gameLoop, 1000 / 30);

/*******************************************************************************
 GAME FUNCTIONS
 *******************************************************************************/
//Functions are a way to perform actions within our code at certain 
//periods of time at can be used to modify the information held within variables//


function gameInitialize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");


    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;

    canvas.width = screenWidth;
    canvas.height = screenHeight;

    document.addEventListener("keydown", keyboardHandler);
    
    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);
    
    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);
    
    playHUD = document.getElementById("playHUD");
    scoreboard = document.getElementById("scoreboard");
    
    image = document.getElementById("source");
    image2 = document.getElementById("source2");
    
   
    GameOver = new Audio("Sounds/GameOver.mp3");
    GameOver.preload = "auto";
    
    Grow = new Audio("Sounds/SnakeGrow.mp3");
    Grow.preload = "auto";
    
    High = new Audio("Sounds/High.mp3");
    High.preload = "auto";
    
    Music = new Audio("Sounds/GetLow.mp3");
    Music.preload = "auto";
    
    Highscore = document.getElementById("Highscore");
    
    setState("PLAY");

}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    HighScore();
    if (gameState === "PLAY") {
       
        snakeUpdate();
        snakeDraw();
        foodDraw(); 
        
    }
}

function gameDraw() {
    /**if(snakeLength >= 10 ){
       
    context.fillStyle = "rgb(0, 0, 0)";
    context.fillRect(0, 0, screenWidth, screenHeight);
    return;
    }
    
    else{ **/
    context.fillStyle = "rgb(255, 0, 102)";
    context.fillRect(0, 0, screenWidth, screenHeight);
    //}
   
}

function gameRestart() {
    snakeInitialize();
    foodInitialize();
    hideMenu(gameOverMenu);
    setState("PLAY"); 
}


/*******************************************************************************
 SNAKE FUNCTIONS
 *******************************************************************************/

function snakeDraw() {
    for (var index = 0; index < snake.length; index++) {      
             
      context.beginPath();
      context.arc(snake[index].x * snakeSize, snake[index].y * snakeSize, 15, 0, 2 * Math.PI, false);
      context.fillStyle = 'purple';
      context.fill();
      context.lineWidth = 2;
      context.strokeStyle = 'black';
      context.stroke();
    }
}


function snakeInitialize() {
    snake = [];
    snakeLength = 1;
    snakeSize = 25;
    snakeDirection = "down";

    for (var index = snakeLength - 1; index >= 0; index--) {
        snake.push({
            x: index,
            y: 0
        });

    }
}


function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;

    if (snakeDirection === "down") {
        snakeHeadY++;
    }
    else if (snakeDirection === "right") {
        snakeHeadX++;
    }

    else if (snakeDirection === "up") {
        snakeHeadY--;
    }

    if (snakeDirection === "left") {
        snakeHeadX--;
    }

    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);

    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);

}



/*******************************************************************************
 FOOD FUNCTIONS
 *******************************************************************************/

function  foodInitialize() {
    food = {
        x: 0,
        y: 0
    };
    setFoodPosition();
}

function foodDraw() {

context.drawImage(image, food.x * snakeSize, food.y * snakeSize, 60, 60);
}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);

    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);

    checkFoodCollisions(randomX, randomY);
}

/*******************************************************************************
 INPUT FUNCTIONS
 *******************************************************************************/

function keyboardHandler() {
    console.log(event);

    if (event.keyCode == "39" && snakeDirection != "left") {
        snakeDirection = "right";
    }

    else if (event.keyCode == "40" && snakeDirection != "up") {
        snakeDirection = "down";
    }

    else if (event.keyCode == "37" && snakeDirection != "right") {
        snakeDirection = "left";
    }

    if (event.keyCode == "38" && snakeDirection != "down") {
        snakeDirection = "up";
    }
}


/*******************************************************************************
 COLLISION HANDLING
 *******************************************************************************/

function checkFoodCollisions(snakeHeadX, snakeHeadY, randomX, randomY) {
    if (snakeHeadX == food.x && snakeHeadY == food.y || snakeHeadX+1 == food.x && snakeHeadY+1 == food.y || snakeHeadX-1 == food.x && snakeHeadY-1 == food.y) {
        snake.push({
            x: 0,
            y: 0
        });
        
        Grow.play();
        snakeLength++;

        setFoodPosition();


    }
   
}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0
        || snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        
        Music.pause();  
        GameOver.play();
        setState("GAME OVER");
    }
}

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for(var index = 1; index <  snake.length; index++){
        if(snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
            
            Music.pause();
            GameOver.play();
            setState("GAME OVER");
            return;
        }
    }
}

/*******************************************************************************
 GAME STATE HANDLING
 *******************************************************************************/

function setState(state) {
    gameState = state;
    showMenu(state);
}

/*******************************************************************************
MENU FUNCTIONS
 *******************************************************************************/

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";
}

function showMenu(state) {
    if(state == "GAME OVER"){
       displayMenu(gameOverMenu);
       Music.pause();
    }
    else if( state == "PLAY"){
        displayMenu(playHUD);
        Music.play();
    }
    
    else if (state == "PREGAME"){
       displayMenu(playMenu); 
    }
}

function centerMenuPosition(menu) {
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";    
}
     
function drawScoreboard(){
  scoreboard.innerHTML = "Snake Length: " + snakeLength;
  Highscore.innerHTML = "Highscore: " + localStorage.getItem(HighscoreX);
}
 function HighScore () {
     if(localStorage.getItem(HighscoreX) < snakeLength) {
         localStorage.setItem(HighscoreX , snakeLength);
          High.play();
     }
 }
     
